A group of LaTeX models
=======================

This repository is intended to have a group of LaTeX preformatted models to aid and boost people to document their work.

**Latex for beginner/howto:**
http://en.wikibooks.org/wiki/LaTeX

**Questions and doubts:**
http://tex.stackexchange.com

**The not so short introduction to LaTeX2:**
http://tobi.oetiker.ch/lshort/lshort.pdf

**Other good options:**
http://tex.stackexchange.com/questions/11/what-is-the-best-book-to-start-learning-latex

**Because the document itself is important too:** http://www-mech.eng.cam.ac.uk/mmd/ashby-paper-V6.pdf
